#-------------------------------------------------
#
# Project created by QtCreator 2018-05-11T16:43:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QrencodeQt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

DEFINES += HAVE_CONFIG_H

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    qrencode/split.c \
    qrencode/rsecc.c \
    qrencode/qrspec.c \
    qrencode/qrinput.c \
    qrencode/qrencode.c \
    qrencode/qrenc.c \
    qrencode/mqrspec.c \
    qrencode/mmask.c \
    qrencode/mask.c \
    qrencode/bitstream.c


HEADERS += \
        mainwindow.h \
    qrencode/split.h \
    qrencode/rsecc.h \
    qrencode/qrspec.h \
    qrencode/qrinput.h \
    qrencode/qrencode.h \
    qrencode/qrencode_inner.h \
    qrencode/mqrspec.h \
    qrencode/mmask.h \
    qrencode/mask.h \
    qrencode/bitstream.h \
    qrencode/config.h


FORMS += \
        mainwindow.ui

CONFIG += mobility
MOBILITY = 

